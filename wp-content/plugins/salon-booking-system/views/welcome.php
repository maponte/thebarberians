<link rel="stylesheet" type="text/css" href="../css/welcome.css">

<?php
/**
* Welcome Page View
*
* @since 1.0.0
* @package WPW
*/
if ( ! defined( 'WPINC' ) ) {
die;
}
?>
<div class="wrap about-wrap">
    <h1><?php printf( __( 'Salon Booking System', 'WPW' ), WPW_VERSION ); ?></h1>
    <div class="about-text">
        <h4><?php printf( __( "Welcome!<br /> follow these simple steps to start your new booking expericence.", 'WPW' ), WPW_VERSION ); ?></h4>
    </div>
    <div class="wp-badge welcome__logo"></div>
    <div class="feature-section two-col">
        <div class="col">
            <h3><?php _e( 'Step 1 - General settings', 'WPW' ); ?></h3>
            <ul>
                <li><?php _e( 'Go to <span>Salon > Settings > General</span> tab and fill out these minimum required options:&ensp;', 'WPW' ); ?><a href="admin.php?page=salon-settings&tab=general"><?php _e( 'BRING ME THERE', 'WPW' ); ?></a></li>
                <li><?php _e( 'Fill out your salon information', 'WPW' ); ?></li>
                <li><?php _e( 'Scroll down the page and setup the “Booking notes”', 'WPW' ); ?></li>
                <li><?php _e( 'Then setup the “Assistant selection” options', 'WPW' ); ?></li>
            </ul>
            <?php _e( 'The rest of the options are optional', 'WPW' ); ?>
            
            <br>
            <br>
            
            <h3><?php _e( 'Step 2 - Booking rules', 'WPW' ); ?></h3>
            <ul>
                <li><?php _e( 'Go to <span>Salon > Settings > Booking rules</span> tab&ensp;', 'WPW' ); ?><a href="admin.php?page=salon-settings&tab=booking"><?php _e( 'BRING ME THERE', 'WPW' ); ?></a></li>
                <li><?php _e( 'Setup “Customers per session” option, how many customers can be attended at the same times', 'WPW' ); ?></li>
                <li><?php _e( 'Setup “Session average duration” option, that changes the minimum hour fraction used by customers to select the timing of their reservation and for you to define the duration of your services', 'WPW' ); ?></li>
                <li><?php _e( '“Change order” option, can be enabled if you want the booking process starting from the selection of the services, then the assistants, and then the date/time', 'WPW' ); ?></li>
                <li><?php _e( '“Setup “Online booking available days”, this option is very important as it should represents your real weekly time table', 'WPW' ); ?></li>
            </ul>
            <?php _e( 'The rest of the options are optional', 'WPW' ); ?>
        </div>
        <div class="col">
            <h3><?php _e( 'Step 3 - Services', 'WPW' ); ?></h3>
            <ul>
                <li><?php _e( 'Go to <span>Salon > Services</span> section and create as many services as you need&ensp;', 'WPW' ); ?><a href="edit.php?post_type=sln_service"><?php _e( 'BRING ME THERE', 'WPW' ); ?></a></li>
            </ul>
            
            <h3><?php _e( 'Step 4 - Assistants', 'WPW' ); ?></h3>
            <ul>
                <li><?php _e( 'Got to <span>Salon > Assistants</span> section and create as many assistants as you need just in case you are not working alone&ensp;', 'WPW' ); ?><a href="edit.php?post_type=sln_attendant"><?php _e( 'BRING ME THERE', 'WPW' ); ?></a></li>
            </ul>
     
                <h3><?php _e( 'Step 5 - Testing', 'WPW' ); ?></h3>
                <ul>
                    <li><?php _e( 'Open the <span>Booking</span> page on front-end and make some test with the booking form and verify that the booking process is correct', 'WPW' ); ?></li>
                </ul>

                <h3><?php _e( 'More documentation?', 'WPW' ); ?></h3>
                <ul>
                    <li><?php _e( 'You can consult the <span>Salon > Settings > Support</span> tab where you can find all the info you need or get in contact with us&ensp;', 'WPW' ); ?><a href="admin.php?page=salon-settings&tab=documentation"><?php _e( 'BRING ME THERE', 'WPW' ); ?></a></li>
                </ul>
            </div>
        </div>
        
    </div>