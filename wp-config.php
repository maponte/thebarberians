<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wptest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4c!gMi3qb| LiC^By]n~,Ze1cVC$l,s_4;IR3!~{r3o&<VpmJzdVj1`2HC}K@p5B' );
define( 'SECURE_AUTH_KEY',  '4XeB{ Gk|0hHE#q+3r!BVA}[bB3u$Is=e//(2p3WIaHVwx60QTLpWRd8Z=lt:=37' );
define( 'LOGGED_IN_KEY',    '>7yfwm;p0% uuuen,&c.aCB0jM`fjnLgxb=.exkK(X1V0^q)K8;)`<mIMW3vnd8W' );
define( 'NONCE_KEY',        'clA)J)3FRJ5DH ]kl:|+zc1+_,{;.?.[BDb~>Uc{]2;{JR->Q5EDz*n$(hT]>Gec' );
define( 'AUTH_SALT',        '9wV-YqIe<2wgsK(z0zm+>=49N^%=LlA2s~b9ga2u-isvzvu|}?1+RiFn3$U}nmrd' );
define( 'SECURE_AUTH_SALT', 'n.Nk~8@Zc6xC2[9vPU`fHl#{N3t:BfE:5t|jpG$9Zfbn!&KmV; !PaCwAe!>h3PI' );
define( 'LOGGED_IN_SALT',   '2~W$#.!;&3D{=Bx2LPpN%mcon*,ObIv?eFqO/cl5.L6z~/C8YmzK:|ngTHm>jbyL' );
define( 'NONCE_SALT',       ' U8*OeceK!uDxgs7sMbz$uHX>JUSV7DC}XQQc3k[1Y=cy 1Ig.qyFE(q ,SfieN ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
